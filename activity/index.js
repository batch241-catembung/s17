/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
		
	//first function here:
	function user(){
		let fullName = prompt("What is your name: ");
		let age = prompt("What is your age: ");
		let address = prompt("Where do you live: ");

		console.log("Hello, " + fullName); 
		console.log("You are " + age + " years old."); 
		console.log("You live in " + address); 
	};
	user();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBand(){

		console.log("Favorite Band are the following: ");
		console.log("1. Westlife");
		console.log("2. Kamikazee");
		console.log("3. Backstreet Boys");
		console.log("4. NSYNC ");
		console.log("5. Parokya ni Edgar");

	};
	favoriteBand();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovie(){

		console.log("Favorite Movies and ratings are the following: ");

		console.log("1. vivarium");
		console.log("Rotten Tomatoes Rating: 39%");

		console.log("2. Puss in Boots: The Last Wish");
		console.log("Rotten Tomatoes Rating: 93%");

		console.log("3. Everthing Everywhere All at Once");
		console.log("Rotten Tomatoes Rating: 89%");

		console.log("4. LUCY");
		console.log("Rotten Tomatoes Rating: 47%");

		console.log("5. The Invisible Man");
		console.log("Rotten Tomatoes Rating: 88%");

	};
	favoriteMovie();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

 //printUsers();
let printFriends /*()*/ = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);
