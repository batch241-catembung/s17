// alert("hello!");

/*
	Function
		function in javascript are lines/block of codes that tell our devicesto perform a certain task whren called

		fuction are mostly created complicated task to run several line of code in succession

		function are also used to prevent repeating line/block of codes that perform the same task
*/

// fuction statement 
	// (function statement) defines a function with the specified parameters


/*

	syntax 
	function functionName(){
		codeblock(statement)
	};
	
	function keyword - used to defined a a javascript functions 
	functionName - the function name. Function named to be avle to used later in the code

	function block({}) - the statement which compriose the body of the function. this is where the code to be exectued 

*/
console.log("My name is John outside!");
function printName(){
	console.log("My name is John!");
}


//function invocation
/*
	the code block and tetaemens inside a function is not immediatley exectued when the function is define. The code block and statement inside a function is exectued when the function is invoked or called.

*/
// invoke call function that we declared 
printName();

// function declaration  vs function expression 
	// function declaration 
			// a function can ber created through declared by using the  fucntion keyword and adding function name
			// declared  function are not exectued immediately. they are saved for later used and will exectued later when they are invoked(called)
		
		// hoisting
		declaredFunction();
		function declaredFunction(){
			console.log("hello world from declaredFunction()")
		};
			declaredFunction();

	// function expression
		// a function  can also be stored in a variable. thes is called function expression

		// a function expression is an anonymous function assigned to the variableFunction

		// anonymous function - function without a name



	let variableFunction = function(){
		console.log("hello from the variableFunction");
	};

	variableFunction();
// funcion expression 
	let fucExpression = function fucName(){
		console.log("hello from the other side");
	}

	fucExpression();
	//fucName //error

// can we reassign declared functions and function expression to a new anonymous functions?
	declaredFunction = function(){
		console.log("updated declaredFunction");
	};
	declaredFunction();

	fucExpression = function(){
		console.log("updated fucExpression");
	};

	fucExpression();

	// we cannot reassign a  function expressio initialized with  const
	const constanFuc = function(){
		console.log("initialized with const");
	};
	constanFuc();
	
	//  constanFuc = function(){
	// 	console.log("can we reassign");
	// };
	// constanFuc();

// function scoping 
/*
	scope is the accessibility (visibility) of variables 

	JS variables has 3 types of scope 
		1.local/block scope
		2.global scope
		3. function scope	
*/

{
	let localVar = "i am a local variable"
}

let globalVar = "i am glbal variable"
console.log(globalVar)
//console.log(localVar) //index.js:116 Uncaught ReferenceError: localVar is not defined

function showNames(){

	// function scope variables
	var funtionVar ="joe";
	const functionConst = "john";
	let functionlet = "jane";

	console.log(funtionVar);
	console.log(functionConst);
	console.log(functionlet);
};
	showNames();

  	// the variables  funtionVar, functionConst, functionlet are function scope variables, mean can only be accessed inside to the function they were declared in

// nested function
	// you can create another function. this is called a nested function. thies nested funtion being insede tyhe myNewFunction will have access to the variable "name" as they are within the same code scope
	function myNewFunction(){
		let name = "maria";

		function nestedFunction(){
			let nestedName ="jose";
			console.log(name, nestedName);
		};
		nestedFunction();
	};

	myNewFunction();

// function and global scope variables
	// global variable
	let globalName = "erven josh";

	function myNewFunction2 (){
		let nameInside ="jenno";
		console.log(globalName);
	};

myNewFunction2();
	


// using alert method()
	/*
	syntax:
		alert(message);
	*/

	// alert("hello world");

	function showSampleAlert(){
		alert("hello user");
	}

	// showSampleAlert();

console.log("i will only log in the console  when the alert is dismissed");

// using prompt()

	/*
		syntax:
			prompt("dialog");
	*/


	// let samplePrompt = prompt("enter your name");
	// console.log("hello "+ samplePrompt);
	// console.log(typeof samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("enter your first name");
	let lastName = prompt("enter your last name");

	console.log("hello "+ firstName + " "+lastName);
	// console.log("welcome to my page");


};
// printWelcomeMessage();


// function naming conventions
	// name your function in small caps. followed by camelCase when naming varibles and function

	function displayCarInfo(){
		console.log("brand: toyota");
		console.log("type: sedan");
		console.log("price: 1, 500, 000");

	}
	displayCarInfo();

	//function name should be definitive of the task it will perform. it usually contains a verb

	function getCourses(){
		let course = ["science 101", "english 101", "math 101"];
		console.log(course);
	};
	getCourses();












